# Private Game Projects


## Game Projects I created

| App name | Modes | Ongoing | UI-Kit |
|:---|:-:|:-:|:-:|
| [1 MineSweeper](#minesweeper)                  | - 💁‍♂️ - |   |   |
| [2 Pentago](#pentago)                          | - 👬 - |   |   |
| [3 Quarto](#quarto)                            | - 👬 - |   |   |
| [4 Alchemy](#alchemy)                          | - 💁‍♂️ - | - ⏳ - |   |
| [5 ChainReaction](#chainreaction)              | - 👬 - 👪 - 👨‍👩‍👧‍👦 - |   |   |
| [6 MakeSquare](#makesquare)                    | - 👬 - 👪 - 👨‍👩‍👧‍👦 - |   |   |
| [7 2048](#2048)                                | - 💁‍♂️ - |   |   |
| [8 Tic Tac Toe](#tic-tac-toe)                  | - 👬 - 🤖 - |   |   |
| [9 Line Connect](#line-connect)                | - 👬 - 👪 - 👨‍👩‍👧‍👦 - |   |   |
| [10 Nine Marbles](#nine-marbles)               | - 👬 - |   |   |
| [11 Color Choose Game](#color-choose-game)     | - 💁‍♂️ - |   |   |
| [12 SOS](#sos)                                 | - 👬 - 👪 - 👨‍👩‍👧‍👦 - |   |   |
| [13 Snake-n-Ladder](#snake-n-ladder)           | - 👬 - 👪 - 👨‍👩‍👧‍👦 - |   |   |
| [14 Towers](#towers)                           | - 💁‍♂️ - |   |   |
| [15 Undead](#undead)                           | - 💁‍♂️ - |   |   |
| [16 Lightout](#lightout)                       | - 💁‍♂️ - | - ⏳ - |   |
| [17 Match3](#match3)                           | - 💁‍♂️ - | - ⏳ - |   |
| [18 RPG](#rpg)                                 | - 💁‍♂️ - | - ⏳ - |   |
| [19 thoho](#thoho)                             | - 🌏 - | - ⏳ - |   |
| [20 Bingo](#bingo)                             | - 🌏 - | - ⏳ - |   |
| [21 Color Sort](#color-sort)                   | - 💁‍♂️ - | - ⏳ - |   |
| [22 LaserMaze Solver](#lasermazesolver)        | - 🧠 - |   | - ✅ - |
| [23 Nonogram Solver](#nonogram-solver)         | - 🧠 - | - ⏳ - |   |
| [24 MatchTheTiles Solver](#match-tiles-solver) | - 🧠 - |   |   |
| [25 Sudoku Smart Solver](#sudoku-solver)       | - 🧠 - | - ⏳ - |   |
| [26 GOL Auto](#gol-auto)                       | - 0️⃣ - |   |   |


## My Private Game Projects

![Game Projects](/Game-Projects.png?raw=true)


# All the video & pic of games

##  Tic Tac Toe
| Home Page | Easy | Medium | Hard | Extreme | Extreme | Extreme | Extreme | 
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| <img src="TicTacToe/TicTacToeHome.png?raw=true" width="150" height="200" title="Trending"> | <img src="TicTacToe/TicTacToeEasy.png?raw=true" width="150" height="200" title="Trending"> | <img src="TicTacToe/TicTacToeMedium.png?raw=true" width="150" height="200" title="Trending"> | <img src="TicTacToe/TicTacToeHard.png?raw=true" width="150" height="200" title="Trending"> | <img src="TicTacToe/TicTacToe00.png?raw=true" width="150" height="200" title="Trending"> | <img src="TicTacToe/TicTacToe01.png?raw=true" width="150" height="200" title="Trending"> | <img src="TicTacToe/TicTacToe02.png?raw=true" width="150" height="200" title="Trending"> | <img src="TicTacToe/TicTacToe03.png?raw=true" width="150" height="200" title="Trending"> | 

Extreme:  
-  Clickable: Yellow  
- Circle:Blue  
- Cross:Red  
- Draw:Red

<hr style = "background-color: red">


## Color Choose Game
| Home Page | Easy | Medium | Hard | Extreme | Max | 
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| <video src="Color%20Choose%20Game/OddColorPickGameHard.mov" width="150" height="200" type="video/mov" autoplay loop></video> | <img src="Color Choose Game/OddColorPickGame00.png" width="150" height="200" title="Trending"> | <img src="Color Choose Game/OddColorPickGame00.png" width="150" height="200" title="Trending"> | <img src="Color Choose Game/OddColorPickGame00.png" width="150" height="200" title="Trending"> | <img src="Color Choose Game/OddColorPickGame00.png" width="150" height="200" title="Trending"> | <img src="Color Choose Game/OddColorPickGame00.png" width="150" height="200" title="Trending"> |

<hr style = "background-color: red">

## LaserMazeSolver 
**(UIkit)**
| Home Page | Easy | Medium | Hard | Extreme | Extreme | Extreme | Extreme | 
|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|

<hr style = "background-color: red">
<hr style = "background-color: red">
